# Planning

|      | Date       | Focus                                                        |
| ---- | ---------- | ------------------------------------------------------------ |
| 1    | 17.09.2019 | Introduce **objectives** and **guidelines**. <br />Present and experiment with **rapid cycle workflow**. <br />Create **groups** and **ideate**. Describe and assign **roles** within the teams.<br />At the end of the session, **every group must be able to present the project idea**. Every team member must have used the rapid workflow to write a document where he/she describes his perspective on the project idea. |
| 2    | 24.09.2019 | Define the **long term** vision and the **MVP** scope (the goal is to deliver the MVP at the end of the semester). <br />Specify the **scenario** that will be used to **demo the product** at the end of the semester. <br />Create a **landing page** to present the product idea. |
| 3    | 01.10.2019 | **Refine the demo scenario** and create **mockups** for the UI/UX.<br />Decide what **interface**(s) (dekstop only? desktop + mobile?) will be implemented. |
| 4    | 08.10.2019 | **First presentation**. The person responsible for the UI/UX presents the product idea and UI mockups in **10 minutes**. <br />The landing page is up and running. The GitLab repo is clean. <br />**Architecture and planning for the first development iteration**. Pick the **technology stack** (what will be used in the backend, in the frontend and to implement the build pipeline). For instance: Springboot in the backend, Vue.js in the front-end and GitLab pipelines for the CI. **Pick a simple functional slice of the system to be implemented in 3 weeks**. |
| 5    | 15.10.2019 | Development                                                  |
|      |            |                                                              |
| 6    | 29.10.2019 | Development                                                  |
| 7    | 05.11.2019 | Development                                                  |
| 8    | 12.11.2019 | Development                                                  |
| 9    | 19.11.2019 | **Second presentation**. In 10 minutes, the person in charge of DevOps presents the structure of the GitLab repo, explains how the back-end and front-end is automatically built and executed locally. The person also explains which automated tests have been created by the back-end and front-end developers and how they are run in the pipeline. |
| 10   | 26.11.2019 | Development                                                  |
| 11   | 03.12.2019 | Development                                                  |
| 12   | 10.12.2019 | Development                                                  |
| 13   | 17.12.2019 | Development **[CODE FREEZE on 20.12.2019]** - repos and code will be evaluated after that date |
|      |            |                                                              |
|      |            |                                                              |
| 14   | 07.01.2020 | Preparation of the final demo and documentation              |
| 15   | 14.01.2020 | Preparation of the final demo and documentation              |
| 16   | 21.01.2020 | **Final presentations**. In 10 minutes, the person in charge of the front-end presents the product demo (pitch) and describes the implementation of the front-end. In 10 minutes, the person in charge of the back-end explains how the CI pipeline (implemented by the DevOps specialist) is now used to deploy new releases in a cloud environment. |
|      |            |                                                              |

## Workload

"Périodes encadrées: 16 x 4 = 64 périodes, soit 48 heures."

"Charge de travail totale: 90 heures (soit 42 heures en plus des périodes encadrées)."

90 hours ~= 2 weeks of work full time

* I have 2 weeks to implement a back-end, where I expose a domain model via a REST API. I need time to design the model, the API. I need time to write automated tests.
* I have 2 weeks to implement the front-end for my application (mobile and/or desktop). I need to create atomic elements, which I can assemble in larger components and finally in pages. I need to manage the navigation between pages. I need to decide what kind of tests will make the development process efficient.
* I have 2 weeks to design and implement two runtime environments: one local and one in the cloud. I have to implement a CI/CD pipeline to automatically build and test the software, before releasing it into the cloud.
* I have 2 weeks to help my team mates stay deliver what is required to run the demo that will convey the vision of our product. Depending on the situation, I might spend time working in the back-end, the front-end or the runtime environments. But I need to maintain a global view on the progress of the project.

